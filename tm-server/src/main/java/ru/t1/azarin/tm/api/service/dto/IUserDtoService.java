package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.UserDto;
import ru.t1.azarin.tm.enumerated.Role;

import java.util.List;

public interface IUserDtoService extends IDtoService<UserDto> {

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    List<UserDto> findAll();

    @Nullable
    UserDto findByLogin(@Nullable String login);

    @Nullable
    UserDto findByEmail(@Nullable String email);

    @Nullable
    UserDto findOneById(@Nullable String id);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    void removeByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    void updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    );

}
