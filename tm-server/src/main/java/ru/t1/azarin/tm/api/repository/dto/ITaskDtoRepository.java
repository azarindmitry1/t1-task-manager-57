package ru.t1.azarin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.enumerated.Sort;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDto> {

    @Nullable
    List<TaskDto> findAll(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}