package ru.t1.azarin.tm.api.service.model;

import org.jetbrains.annotations.Nullable;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    void unbindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}
