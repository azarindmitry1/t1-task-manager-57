package ru.t1.azarin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.repository.model.IRepository;
import ru.t1.azarin.tm.api.service.model.IService;
import ru.t1.azarin.tm.model.AbstractModel;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
