package ru.t1.azarin.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.repository.model.ISessionRepository;
import ru.t1.azarin.tm.api.service.model.ISessionService;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    public ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @Override
    public void add(@Nullable final Session model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            return sessionRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            return sessionRepository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final Session model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        if (session == null) throw new EntityNotFoundException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final Session model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
