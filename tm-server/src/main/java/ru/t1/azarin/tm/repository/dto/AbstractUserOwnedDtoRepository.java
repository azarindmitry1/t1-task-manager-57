package ru.t1.azarin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.azarin.tm.comparator.CreatedComparator;
import ru.t1.azarin.tm.comparator.NameComparator;
import ru.t1.azarin.tm.dto.model.AbstractUserOwnedDtoModel;

import java.util.Comparator;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel> extends AbstractDtoRepository<M>
        implements IUserOwnedDtoRepository<M> {

    @NotNull
    protected String getSortColumn(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else return "status";
    }

}
