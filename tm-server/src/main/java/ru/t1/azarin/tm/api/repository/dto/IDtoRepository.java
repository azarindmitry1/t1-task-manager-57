package ru.t1.azarin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.model.AbstractDtoModel;

import javax.persistence.EntityManager;

public interface IDtoRepository<M extends AbstractDtoModel> {

    void add(@NotNull M model);

    void remove(@NotNull M model);

    void update(@NotNull M model);

    @NotNull
    EntityManager getEntityManager();

}
