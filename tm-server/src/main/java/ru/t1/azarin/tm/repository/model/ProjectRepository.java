package ru.t1.azarin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.api.repository.model.IProjectRepository;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.model.Project;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<Project> projects = findAll(userId);
        if (projects == null) return;
        for (@NotNull final Project project : projects) entityManager.remove(project);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        @NotNull final String jpql = String.format("SELECT m FROM Project m WHERE m.user.id = :userId ORDER_BY m.%s",
                getSortColumn(sort.getComparator()));
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

}
